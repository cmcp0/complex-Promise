function otherAsyncFunction(id_group, callback) {

            function mIterator(array, index, arrayState) {
                return new Promise(function (resolve, reject) {

                    if (index < array.length) {
                        if (array[index]['hasProperty'] == undefined) {
                            return otherAsyncFunction(array[index].id).then(function (value) {
                                    arrayState = arrayState.concat(value);

                            })
                            .then(function () {
                                return mIterator(array, index + 1, arrayState)
                                .then(function (value) {
                                    return (value);
                                });
                            }).then(function (v) {
                                resolve(v);
                            });
                        } else {
                            arrayState.push(array[index]);
                            return mIterator(array, index + 1, arrayState)
                            .then(function (value) {
                                resolve(value);

                            });
                        }
                    } else {
                        resolve(arrayState);
                    }
                });
            }

            return new Promise(function (Response, Reject) {

               asyncFunction().then(function(result){
                    Response(result);
               });
            });

        }